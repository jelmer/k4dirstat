Source: k4dirstat
Section: misc
Priority: optional
Maintainer: Jerome Robert <jeromerobert@gmx.com>
Build-Depends: debhelper (>= 9.0), pkg-kde-tools, cmake, zlib1g-dev, extra-cmake-modules,
 libkf5xmlgui-dev, libkf5doctools-dev, libkf5kio-dev
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/jeromerobert-guest/k4dirstat
Vcs-Git: https://salsa.debian.org/jeromerobert-guest/k4dirstat.git
Homepage: http://bitbucket.org/jeromerobert/k4dirstat

Package: k4dirstat
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: xdg-utils
Description: graphical disk usage display with cleanup facilities
 K4DirStat (KDE Directory Statistics) is a small utility program that sums
 up disk usage for directory trees, very much like the Unix 'du' command.
 It displays the disk space used up by a directory tree, both numerically
 and graphically.  It is network transparent (i.e., you can use it to sum
 up FTP servers), and comes with predefined and user configurable cleanup
 actions.  You can directly open a directory branch in Konqueror or the
 shell of your choice, compress it to a .tar.bz2 archive, or define your
 own cleanup actions.
